package com.betvictor.text;

import java.util.Comparator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.stream.Collectors;

import com.betvictor.randomtext.ProcessedRandomText;

public class TextResponse {
	private String freqWord;
	private double avgParagraphSize;
	private long avgParagraphProcessingTime;
	private long totalProcessingTime;
	
	public TextResponse() {
		super();
	}
	
	public TextResponse(ProcessedRandomText processedRandomText) {
		this();
		//Calculate freqWord
		if (processedRandomText.getWords() != null) {
			Optional<Entry<String, Long>> mostFreqEntry = processedRandomText.getWords().stream()
					.collect(Collectors.groupingBy(w -> w, Collectors.counting())) // Count the frequency of each word
					.entrySet().stream().max(Comparator.comparingLong(Map.Entry::getValue)); // Get the word with higher frequency
			this.setFreqWord(mostFreqEntry.isPresent() ? mostFreqEntry.get().getKey() : null);
		}
		//Calculate averages
		if (processedRandomText.getNumberOfParagraphs() > 0) {
			if (processedRandomText.getWords() != null) {
				this.setAvgParagraphSize(
						(double)processedRandomText.getWords().size() / processedRandomText.getNumberOfParagraphs());
			}
			this.setAvgParagraphProcessingTime(
					processedRandomText.getProcessingTime() / processedRandomText.getNumberOfParagraphs());
		}
	}
	
	public String getFreqWord() {
		return freqWord;
	}
	public void setFreqWord(String freqWord) {
		this.freqWord = freqWord;
	}
	public double getAvgParagraphSize() {
		return avgParagraphSize;
	}
	public void setAvgParagraphSize(double avgParagraphSize) {
		this.avgParagraphSize = avgParagraphSize;
	}
	public long getAvgParagraphProcessingTime() {
		return avgParagraphProcessingTime;
	}
	public void setAvgParagraphProcessingTime(long avgParagraphProcessingTime) {
		this.avgParagraphProcessingTime = avgParagraphProcessingTime;
	}
	public long getTotalProcessingTime() {
		return totalProcessingTime;
	}
	public void setTotalProcessingTime(long totalProcessingTime) {
		this.totalProcessingTime = totalProcessingTime;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (avgParagraphProcessingTime ^ (avgParagraphProcessingTime >>> 32));
		long temp;
		temp = Double.doubleToLongBits(avgParagraphSize);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((freqWord == null) ? 0 : freqWord.hashCode());
		result = prime * result + (int) (totalProcessingTime ^ (totalProcessingTime >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TextResponse other = (TextResponse) obj;
		if (avgParagraphProcessingTime != other.avgParagraphProcessingTime)
			return false;
		if (Double.doubleToLongBits(avgParagraphSize) != Double.doubleToLongBits(other.avgParagraphSize))
			return false;
		if (freqWord == null) {
			if (other.freqWord != null)
				return false;
		} else if (!freqWord.equals(other.freqWord))
			return false;
		if (totalProcessingTime != other.totalProcessingTime)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TextResponse [freqWord=" + freqWord + ", avgParagraphSize=" + avgParagraphSize
				+ ", avgParagraphProcessingTime=" + avgParagraphProcessingTime + ", totalProcessingTime="
				+ totalProcessingTime + "]";
	}
	
}
