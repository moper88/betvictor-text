package com.betvictor.text;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;


@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
public class TextControllerTest {

	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	private TextDelegate delegate;
	
	@Test
	public void shouldReturnEmptyResponse() throws Exception {
		TextResponse expectedResponse = new TextResponse();
		given(delegate.getTextResponse(1, 5, 10, 20)).willReturn(expectedResponse);
		
		MvcResult result = mockMvc.perform(get("/text")
					.param("p_start", "1")
					.param("p_end", "5")
					.param("w_count_min", "10")
					.param("w_count_max", "20")).
				andExpect(status().isOk()).andReturn();
		
		String response = result.getResponse().getContentAsString();
		assertNotNull(response);
		assertEquals("{\"freq_word\":null,\"avg_paragraph_size\":0.0,\"avg_paragraph_processing_time\":0,\"total_processing_time\":0}", response);
	}
	
	@Test
	public void shouldReturnResponse() throws Exception {
		TextResponse expectedResponse = new TextResponse();
		expectedResponse.setFreqWord("much");
		expectedResponse.setAvgParagraphSize(15);
		expectedResponse.setAvgParagraphProcessingTime(5000);
		expectedResponse.setTotalProcessingTime(15000);
		given(delegate.getTextResponse(1, 5, 10, 20)).willReturn(expectedResponse);
		
		MvcResult result = mockMvc.perform(get("/text")
					.param("p_start", "1")
					.param("p_end", "5")
					.param("w_count_min", "10")
					.param("w_count_max", "20")).
				andExpect(status().isOk()).andReturn();
		
		String response = result.getResponse().getContentAsString();
		assertNotNull(response);
		assertEquals("{\"freq_word\":\"much\",\"avg_paragraph_size\":15.0,\"avg_paragraph_processing_time\":5000,\"total_processing_time\":15000}", response);
	}
	
	@Test
	public void shouldReturnEmptyHistory() throws Exception {
		List<TextResponse> expectedList = new ArrayList<>();
		given(delegate.getHistory()).willReturn(expectedList);
		
		MvcResult result = mockMvc.perform(get("/history")).
				andExpect(status().isOk()).andReturn();
		
		String response = result.getResponse().getContentAsString();
		assertNotNull(response);
		assertEquals("[]", response);
	}
	
	@Test
	public void shouldReturnHistory() throws Exception {
		List<TextResponse> expectedList = new ArrayList<>();
		TextResponse expectedResponse = new TextResponse();
		expectedResponse.setFreqWord("much");
		expectedResponse.setAvgParagraphSize(15);
		expectedResponse.setAvgParagraphProcessingTime(5000);
		expectedResponse.setTotalProcessingTime(15000);
		expectedList.add(expectedResponse);
		given(delegate.getHistory()).willReturn(expectedList);
		
		MvcResult result = mockMvc.perform(get("/history")).
				andExpect(status().isOk()).andReturn();
		
		String response = result.getResponse().getContentAsString();
		assertNotNull(response);
		assertEquals("[{\"freq_word\":\"much\",\"avg_paragraph_size\":15.0,\"avg_paragraph_processing_time\":5000,\"total_processing_time\":15000}]", response);
	}
}
