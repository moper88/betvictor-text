package com.betvictor.text;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@EnableAutoConfiguration
public class TextController {
	
	private static Logger logger = LoggerFactory.getLogger(TextController.class);
	
	private TextDelegate textDelegate;
	
	public TextController(TextDelegate textDelegate) {
		super();
		this.textDelegate = textDelegate;
	}

	/**
	 * Generate random text from <pStart> paragraphs to <pEnd> paragraphs, each paragraphs with words in the range <wCountMin>-<wCountMax>,
	 * and calculate the most frequent word, the average paragraph size, the average time spent analyzing a paragraph 
	 * and the total processing time to generate the final response
	 * @param pStart Indicates the start number of paragraphs
	 * @param pEnd Indicates the end number of paragraphs
	 * @param wCountMin Indicates min number of words in each paragraph
	 * @param wCountMax Indicates max number of words in each paragraph
	 * @return An object with the most frequent word, the average paragraph size, the average time spent analyzing a paragraph 
	 * and the total processing time to generate the final response
	 */
	@RequestMapping(value="/text", method=RequestMethod.GET)
	public TextResponse text(
			@RequestParam("p_start") int pStart, 
			@RequestParam("p_end") int pEnd,
			@RequestParam("w_count_min") int wCountMin,
			@RequestParam("w_count_max") int wCountMax) {
		logger.debug("Requested text for p_start={}, p_end={}, w_count_min={}, w_count_max={}",
				pStart, pEnd, wCountMin, wCountMax);
		return textDelegate.getTextResponse(pStart, pEnd, wCountMin, wCountMax);
	}
	
	/**
	 * Get the last 10 computation results
	 * @return A list with the last 10 computation results, if there are not computations then return empty list
	 */
	@RequestMapping(value="/history", method=RequestMethod.GET)
	public List<TextResponse> history() {
		logger.debug("Requested history");
		return textDelegate.getHistory();
	}
}
