package com.betvictor;
import org.springframework.boot.test.context.ConfigFileApplicationContextInitializer;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;

@Configuration
@ContextConfiguration(classes = {Starter.class}, initializers = ConfigFileApplicationContextInitializer.class)
public class TestDependencies {
	
	public static final int MOCK_PORT = 1080;
	
	public String getServerUri(int port) {
		return "http://localhost:" + port + "/";
	}

}
