package com.betvictor.randomtext;

import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriTemplate;

@Service
public class RandomTextService {
	
	private static Logger logger = LoggerFactory.getLogger(RandomTextService.class);
	
	private RestTemplate restTemplate;
	
	private @Value("${endpoints.random-text-api}") UriTemplate urlRandomTextAPI;
	
	public RandomTextService(RestTemplate restTemplate) {
		super();
		this.restTemplate = restTemplate;
	}
	
	/**
	 * Asynchronous method to process a RandomText API request
	 * @param numberOfParagraphs Indicate how many paragraphs should be generated in the random text
	 * @param minWordsPerSentence Indicate the minimum number of words that should be generated in each paragraph
	 * @param maxWordsPerSentence Indicate the maximum number of words that should be generated in each paragraph
	 * @param globalProcessedRandomText
	 * @return
	 */
	@Async
	public CompletableFuture<ProcessedRandomText> getProcessedRandomTextAsync(
			int numberOfParagraphs, int minWordsPerSentence, int maxWordsPerSentence, ProcessedRandomText globalProcessedRandomText) {
		ProcessedRandomText localProcessedRandomText = getProcessedRandomText(numberOfParagraphs, minWordsPerSentence, maxWordsPerSentence);
		joinProcessedRandomText(globalProcessedRandomText, localProcessedRandomText);
		return CompletableFuture.completedFuture(localProcessedRandomText);
	}
	
	/**
	 * Join the info on <localData> to the existing info on <globalData>
	 * @param globalData Common object for all async tasks
	 * @param localData Info generated inside the async task
	 */
	private synchronized  void joinProcessedRandomText(ProcessedRandomText globalData,
			ProcessedRandomText localData) {
		globalData.setWords(Stream.concat(globalData.getWords().stream(), localData.getWords().stream()).collect(Collectors.toList()));
		globalData.setNumberOfParagraphs(globalData.getNumberOfParagraphs() + localData.getNumberOfParagraphs());
		globalData.setProcessingTime(globalData.getProcessingTime() + localData.getProcessingTime());
	}

	/**
	 * Call to RandomText API to get a word list and an amount of paragraphs
	 * @param numberOfParagraphs Indicate how many paragraphs should be generated in the random text
	 * @param minWordsPerSentence Indicate the minimum number of words that should be generated in each paragraph
	 * @param maxWordsPerSentence Indicate the maximum number of words that should be generated in each paragraph
	 * @return A <ProcessedRandomText> with the word list, the amount of paragraphs and the processing time
	 */
	private ProcessedRandomText getProcessedRandomText(int numberOfParagraphs, int minWordsPerSentence, int maxWordsPerSentence) {
		logger.info("getProcessedRandomText for {} paragraphs, from {} to {} words each", numberOfParagraphs, minWordsPerSentence, maxWordsPerSentence);
		long startTime = System.nanoTime();
		
		RandomTextResponse randomTextResponse = getRandomText(numberOfParagraphs, minWordsPerSentence, maxWordsPerSentence);
		
		List<String> words = getWords(randomTextResponse);
		
		long duration = System.nanoTime() - startTime;
		ProcessedRandomText processedRandomText = new ProcessedRandomText(words, duration, randomTextResponse.getAmount());
		logger.info("Obtained ProcessRandomText: {}", processedRandomText);
		return processedRandomText;
	}
	
	/**
	 * Get list of words from the RandomText API response
	 * @param randomTextResponse the RandomText API response
	 * @return List of words
	 */
	private List<String> getWords(RandomTextResponse randomTextResponse) {
		logger.info("Getting words from : {}", randomTextResponse);
		if (StringUtils.isEmpty(randomTextResponse.getTextOut())) {
			logger.warn("TextOut is empty: {}", randomTextResponse.getTextOut());
			return new ArrayList<>();
		}
		String text = randomTextResponse.getTextOut();
		
		//Deleting paragraph dividers and dots
		text = text.replaceAll("<p>", "")
				.replaceAll("</p>", "")
				.replaceAll("\\.", "");
		return Collections.list(new StringTokenizer(text)).stream()
				.map(token -> (String) token)
				.collect(Collectors.toList());
	}

	/**
	 * Call to RandomText API to get a random text
	 * @param numberOfParagraphs Indicate how many paragraphs should be generated in the random text
	 * @param minWordsPerSentence Indicate the minimum number of words that should be generated in each paragraph
	 * @param maxWordsPerSentence Indicate the maximum number of words that should be generated in each paragraph
	 * @return RandomText API response
	 */
	private RandomTextResponse getRandomText(int numberOfParagraphs, int minWordsPerSentence, int maxWordsPerSentence) {
		//Setting URL
		Map<String, String> uriVariables = new HashMap<>();
		uriVariables.put("numberOfParagraphs", String.valueOf(numberOfParagraphs));
		uriVariables.put("minWords", String.valueOf(minWordsPerSentence));
		uriVariables.put("maxWords", String.valueOf(maxWordsPerSentence));
		URI url = urlRandomTextAPI.expand(uriVariables);
		
		//Setting headers
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		headers.add("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");
		HttpEntity<String> entity = new HttpEntity<>("parameters", headers);
		
		return restTemplate.exchange(url, HttpMethod.GET, entity, RandomTextResponse.class).getBody();
	}
}
