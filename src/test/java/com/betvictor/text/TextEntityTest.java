package com.betvictor.text;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

public class TextEntityTest {

	@Test
	public void shouldCreateEntityWithNullTextResponse() {
		TextEntity entity = new TextEntity(null);
		
		assertNotNull(entity);
		assertNotNull(entity.getDate());
		assertEquals(null , entity.getFreqWord());
		assertEquals(0.0 , entity.getAvgParagraphSize(), 0.1);
		assertEquals(0 , entity.getAvgParagraphProcessingTime());
		assertEquals(0 , entity.getTotalProcessingTime());
	}
	
	@Test
	public void shouldCreateEntityWithEmptyTextResponse() {
		TextResponse textResponse = new TextResponse();
		TextEntity entity = new TextEntity(textResponse);
		
		assertNotNull(entity);
		assertNotNull(entity.getDate());
		assertEquals(null , entity.getFreqWord());
		assertEquals(0.0 , entity.getAvgParagraphSize(), 0.1);
		assertEquals(0 , entity.getAvgParagraphProcessingTime());
		assertEquals(0 , entity.getTotalProcessingTime());
	}
	
	@Test
	public void shouldCreateEntityWithTextResponse() {
		TextResponse textResponse = new TextResponse();
		textResponse.setFreqWord("word");
		textResponse.setAvgParagraphSize(2.5);
		textResponse.setAvgParagraphProcessingTime(1000);
		textResponse.setTotalProcessingTime(5000);
		TextEntity entity = new TextEntity(textResponse);
		
		assertNotNull(entity);
		assertNotNull(entity.getDate());
		assertEquals("word" , entity.getFreqWord());
		assertEquals(2.5 , entity.getAvgParagraphSize(), 0.1);
		assertEquals(1000 , entity.getAvgParagraphProcessingTime());
		assertEquals(5000 , entity.getTotalProcessingTime());
	}
	
	@Test
	public void shouldCreateTextResponseFromEntity() {
		TextResponse textResponse = new TextResponse();
		textResponse.setFreqWord("word");
		textResponse.setAvgParagraphSize(2.5);
		textResponse.setAvgParagraphProcessingTime(1000);
		textResponse.setTotalProcessingTime(5000);
		TextEntity entity = new TextEntity(textResponse);
		TextResponse textResponseFromEntity = entity.toTextResponse();
		
		assertNotNull(textResponseFromEntity);
		assertEquals(textResponse, textResponseFromEntity);
	}
}
