# BetVictor Text - Interview Task

## Requirements

- Java 1.8
- Maven 3

## Setup the application

Execute the next commands to run the application in the folder where the repository has been cloned:
> mvn spring-boot:run

### Endpoints required by the task:

 - http://localhost:8081/betvictor/text?p_start=<p_start>&p_end=<p_end>&w_count_min=<w_count_min>&w_count_max=<w_count_max> : Request to RandomText API and process to get a JSON with the most frequent word, the average size of a paragraph, the average time spent analyzing a paragraph and the total processing time.
 - http://localhost:8081/betvictor/history : Display last 10 computation results

### Endpoints provided by spring boot actuator:

 - http://localhost:8081/betvictor/actuator/info : Displays arbitrary application info.
 - http://localhost:8081/betvictor/actuator/health : Shows application health information (a simple ‘status’ when accessed over an unauthenticated connection or full message details when authenticated)

### Endpoint to access to the H2 DB:

 - http://localhost:8081/betvictor/h2-console : Admins H2 DB

