DROP TABLE IF EXISTS `historic_texts`;
CREATE TABLE `historic_texts` (
  `id` bigint(20) PRIMARY KEY AUTO_INCREMENT,
  `date` datetime NOT NULL,
  `freq_word` varchar(45),
  `avg_paragraph_size` double(20),
  `avg_paragraph_processing_time` bigint(20),
  `total_processing_time` bigint(20)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;