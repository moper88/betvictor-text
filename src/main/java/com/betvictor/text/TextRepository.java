package com.betvictor.text;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface TextRepository extends CrudRepository<TextEntity, Long>{

	public List<TextEntity> findTop10ByOrderByDateDesc();
}
