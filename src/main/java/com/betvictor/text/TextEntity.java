package com.betvictor.text;

import java.time.Instant;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "historic_texts")
public class TextEntity {

	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	private Long id;
	private Instant date;
	private String freqWord;
	private double avgParagraphSize;
	private long avgParagraphProcessingTime;
	private long totalProcessingTime;
	
	public TextEntity() {
		super();
	}
	
	public TextEntity(TextResponse text) {
		super();
		setDate(Instant.now());
		if (text != null) {
			setFreqWord(text.getFreqWord());
			setAvgParagraphSize(text.getAvgParagraphSize());
			setAvgParagraphProcessingTime(text.getAvgParagraphProcessingTime());
			setTotalProcessingTime(text.getTotalProcessingTime());
		}
	}
	
	public TextResponse toTextResponse() {
		TextResponse response = new TextResponse();
		response.setFreqWord(getFreqWord());
		response.setAvgParagraphSize(getAvgParagraphSize());
		response.setAvgParagraphProcessingTime(getAvgParagraphProcessingTime());
		response.setTotalProcessingTime(getTotalProcessingTime());
		return response;
	}
	
	public Instant getDate() {
		return date;
	}
	public void setDate(Instant date) {
		this.date = date;
	}
	public String getFreqWord() {
		return freqWord;
	}
	public void setFreqWord(String freqWord) {
		this.freqWord = freqWord;
	}
	public double getAvgParagraphSize() {
		return avgParagraphSize;
	}
	public void setAvgParagraphSize(double avgParagraphSize) {
		this.avgParagraphSize = avgParagraphSize;
	}
	public long getAvgParagraphProcessingTime() {
		return avgParagraphProcessingTime;
	}
	public void setAvgParagraphProcessingTime(long avgParagraphProcessingTime) {
		this.avgParagraphProcessingTime = avgParagraphProcessingTime;
	}
	public long getTotalProcessingTime() {
		return totalProcessingTime;
	}
	public void setTotalProcessingTime(long totalProcessingTime) {
		this.totalProcessingTime = totalProcessingTime;
	}
}
