package com.betvictor.text;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Arrays;

import org.junit.Test;

import com.betvictor.randomtext.ProcessedRandomText;

public class TextResponseTest {

	@Test
	public void shouldGenerateTextResponse() {
		ProcessedRandomText givenObject = new ProcessedRandomText(
				Arrays.asList("much", "very", "much"),
				1500,
				3);
		TextResponse response = new TextResponse(givenObject);
		
		assertNotNull(response);
		assertEquals("much", response.getFreqWord());
		assertEquals(1.0, response.getAvgParagraphSize(), 0.0001);
		assertEquals(500, response.getAvgParagraphProcessingTime());
		assertEquals(0, response.getTotalProcessingTime());

	}
	
	@Test
	public void shouldGenerateTextResponseWithDecimalAvg() {
		ProcessedRandomText givenObject = new ProcessedRandomText(
				Arrays.asList("much", "very", "much", "very", "much"),
				1600,
				3);
		TextResponse response = new TextResponse(givenObject);
		
		assertNotNull(response);
		assertEquals("much", response.getFreqWord());
		assertEquals(1.6666, response.getAvgParagraphSize(), 0.0001);
		assertEquals(533, response.getAvgParagraphProcessingTime());
		assertEquals(0, response.getTotalProcessingTime());

	}
	
	@Test
	public void shouldGenerateTextResponseWithEmptyWords() {
		ProcessedRandomText givenObject = new ProcessedRandomText(
				Arrays.asList(),
				1500,
				3);
		TextResponse response = new TextResponse(givenObject);
		
		assertNotNull(response);
		assertEquals(null, response.getFreqWord());
		assertEquals(0.0, response.getAvgParagraphSize(), 0.0001);

	}
	
	@Test
	public void shouldGenerateTextResponseWithNullWords() {
		ProcessedRandomText givenObject = new ProcessedRandomText(
				null,
				1500,
				3);
		TextResponse response = new TextResponse(givenObject);
		
		assertNotNull(response);
		assertEquals(null, response.getFreqWord());
		assertEquals(0.0, response.getAvgParagraphSize(), 0.0001);

	}
	
	@Test
	public void shouldGenerateTextResponseWithZeroParagraphs() {
		ProcessedRandomText givenObject = new ProcessedRandomText(
				null,
				1500,
				0);
		TextResponse response = new TextResponse(givenObject);
		
		assertNotNull(response);
		assertEquals(0.0, response.getAvgParagraphSize(), 0.0001);
		assertEquals(0, response.getAvgParagraphProcessingTime());

	}
}
