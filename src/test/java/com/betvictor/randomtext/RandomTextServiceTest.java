package com.betvictor.randomtext;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;

import com.betvictor.Starter;


@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {Starter.class})
@SpringBootTest
public class RandomTextServiceTest {

	private MockRestServiceServer mockServer;
	
	@Autowired
	private RestTemplate restTemplate;
	
	@Autowired
	private RandomTextService service;

	@Before
	public void setup() {
		mockServer = MockRestServiceServer.bindTo(restTemplate).build();
	}
	
	@Test
	public void shouldGetProcessedRandomText () {
		mockServer.expect(requestTo("http://www.randomtext.me/api/giberish/p-3/2-5"))
		.andRespond(withSuccess("{\"type\":\"gibberish\","
						+ "\"amount\":3,"
						+ "\"number\":\"2\","
						+ "\"number_max\":\"5\","
						+ "\"format\":\"p\","
						+ "\"time\":\"00:51:43\","
						+ "\"text_out\":\"<p>Opossum smiled exotically.<\\/p>\\r<p>Hello wicked jeez.<\\/p>\\r<p>Honey so much.<\\/p>\\r\"}",
						MediaType.APPLICATION_JSON));
		List<String> expectedWords = Arrays.asList("Opossum", "smiled", "exotically", "Hello", "wicked", "jeez", "Honey", "so", "much");
		
		ProcessedRandomText global = new ProcessedRandomText();
		ProcessedRandomText result = service.getProcessedRandomTextAsync(3, 2, 5, global).join();
		
		assertNotNull(result);
		assertEquals(3, result.getNumberOfParagraphs());
		assertNotNull(result.getWords());
		assertEquals(9, result.getWords().size());
		assertTrue(result.getWords().containsAll(expectedWords));
		assertTrue(expectedWords.containsAll(result.getWords()));
		
		assertNotNull(global);
		assertEquals(3, global.getNumberOfParagraphs());
		assertNotNull(global.getWords());
		assertEquals(9, global.getWords().size());
		assertTrue(global.getWords().containsAll(expectedWords));
		assertTrue(expectedWords.containsAll(global.getWords()));
	}
	
	@Test
	public void shouldGetProcessedRandomTextWithEmptyWords () {
		mockServer.expect(requestTo("http://www.randomtext.me/api/giberish/p-3/2-5"))
		.andRespond(withSuccess("{\"type\":\"gibberish\","
						+ "\"amount\":3,"
						+ "\"number\":\"2\","
						+ "\"number_max\":\"5\","
						+ "\"format\":\"p\","
						+ "\"time\":\"00:51:43\","
						+ "\"text_out\":\"\"}",
						MediaType.APPLICATION_JSON));
		List<String> expectedWords = Arrays.asList();
		
		ProcessedRandomText global = new ProcessedRandomText();
		ProcessedRandomText result = service.getProcessedRandomTextAsync(3, 2, 5, global).join();
		
		assertNotNull(result);
		assertEquals(3, result.getNumberOfParagraphs());
		assertNotNull(result.getWords());
		assertEquals(0, result.getWords().size());
		assertTrue(result.getWords().containsAll(expectedWords));
		assertTrue(expectedWords.containsAll(result.getWords()));
		
		assertNotNull(global);
		assertEquals(3, global.getNumberOfParagraphs());
		assertNotNull(global.getWords());
		assertEquals(0, global.getWords().size());
		assertTrue(global.getWords().containsAll(expectedWords));
		assertTrue(expectedWords.containsAll(global.getWords()));
	}
	
	@Test
	public void shouldJoinWithPreviousGlobalData () {
		mockServer.expect(requestTo("http://www.randomtext.me/api/giberish/p-3/2-5"))
		.andRespond(withSuccess("{\"type\":\"gibberish\","
						+ "\"amount\":3,"
						+ "\"number\":\"2\","
						+ "\"number_max\":\"5\","
						+ "\"format\":\"p\","
						+ "\"time\":\"00:51:43\","
						+ "\"text_out\":\"<p>Opossum smiled exotically.<\\/p>\\r<p>Hello wicked jeez.<\\/p>\\r<p>Honey so much.<\\/p>\\r\"}",
						MediaType.APPLICATION_JSON));
		List<String> expectedWords = Arrays.asList( "Opossum", "smiled", "exotically", "Hello", "wicked", "jeez", "Honey", "so", "much");
		List<String> expectedGlobalWords = Arrays.asList("very", "so", "Opossum", "smiled", "exotically", "Hello", "wicked", "jeez", "Honey", "so", "much");
		
		ProcessedRandomText global = new ProcessedRandomText(Arrays.asList("very", "so"), 100, 1);
		ProcessedRandomText result = service.getProcessedRandomTextAsync(3, 2, 5, global).join();
		
		assertNotNull(result);
		assertEquals(3, result.getNumberOfParagraphs());
		assertNotNull(result.getWords());
		assertEquals(9, result.getWords().size());
		assertTrue(result.getWords().containsAll(expectedWords));
		assertTrue(expectedWords.containsAll(result.getWords()));
		
		assertNotNull(global);
		assertEquals(4, global.getNumberOfParagraphs());
		assertNotNull(global.getWords());
		assertEquals(11, global.getWords().size());
		assertTrue(global.getWords().containsAll(expectedGlobalWords));
		assertTrue(expectedGlobalWords.containsAll(global.getWords()));
		assertEquals (100 + result.getProcessingTime(), global.getProcessingTime());
	}
}
