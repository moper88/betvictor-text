package com.betvictor.text;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest()
public class TextRepositoryTest {

	@Autowired
	private TextRepository repository;
	
	@Before
	public void setup() {
		repository.deleteAll();
	}
	
	@Test
	public void shouldGetEmptyList() {
		List<TextEntity> entities = repository.findTop10ByOrderByDateDesc();
		
		assertNotNull(entities);
		assertEquals(0, entities.size());
	}
	
	@Test
	public void shouldGetListWith1Item() {
		repository.save(new TextEntity(null));
		
		List<TextEntity> entities = repository.findTop10ByOrderByDateDesc();
		
		assertNotNull(entities);
		assertEquals(1, entities.size());
	}
	
	@Test
	public void shouldGetListWithOnly10Item() {
		for (int i = 0; i < 11; i++) {
			repository.save(new TextEntity(null));
		}
		
		List<TextEntity> entities = repository.findTop10ByOrderByDateDesc();
		
		assertNotNull(entities);
		assertEquals(10, entities.size());
	}
}
