package com.betvictor.text;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.betvictor.randomtext.ProcessedRandomText;
import com.betvictor.randomtext.RandomTextService;

@Service
public class TextDelegate {
	
	private RandomTextService randomTextService;
	
	private TextRepository textRepository; 
	
	public TextDelegate(RandomTextService randomTextService, TextRepository textRepository) {
		this.randomTextService = randomTextService;
		this.textRepository = textRepository;
	}
	
	/**
	 * Generate random text from <pStart> paragraphs to <pEnd> paragraphs, each paragraphs with words in the range <wCountMin>-<wCountMax>,
	 * and calculate the most frequent word, the average paragraph size, the average time spent analyzing a paragraph 
	 * and the total processing time to generate the final response
	 * @param pStart Indicates the start number of paragraphs
	 * @param pEnd Indicates the end number of paragraphs
	 * @param wCountMin Indicates min number of words in each paragraph
	 * @param wCountMax Indicates max number of words in each paragraph
	 * @return An object with the most frequent word, the average paragraph size, the average time spent analyzing a paragraph 
	 * and the total processing time to generate the final response
	 */
	public TextResponse getTextResponse(int pStart, int pEnd, int wCountMin, int wCountMax) {
		long startTime = System.nanoTime();
		
		ProcessedRandomText globalProcessedRandomText = new ProcessedRandomText();
		//Kick multiple asynchronous processes
		List<CompletableFuture<ProcessedRandomText>> futures = new ArrayList<>();
		for (int p = pStart; p <= pEnd; p++) {
			futures.add(randomTextService.getProcessedRandomTextAsync(p, wCountMin, wCountMax, globalProcessedRandomText));
		}
		
		// Wait until processes are all done
		for (CompletableFuture<ProcessedRandomText> future : futures) {
			future.join();
		}
		
		//Creating response
		TextResponse response = new TextResponse(globalProcessedRandomText);
		long duration = System.nanoTime() - startTime;
		response.setTotalProcessingTime(duration);
		
		//Saving response
		textRepository.save(new TextEntity(response));
		return response;
	}
	
	/**
	 * Get the last 10 computation results
	 * @return A list with the last 10 computation results, if there are not computations then return empty list
	 */
	public List<TextResponse> getHistory() {
		return textRepository.findTop10ByOrderByDateDesc() //Get from DB
				.stream().map(TextEntity::toTextResponse) //Convert entity to TextResponse
				.collect(Collectors.toList()); //Collect to list
	}
}
