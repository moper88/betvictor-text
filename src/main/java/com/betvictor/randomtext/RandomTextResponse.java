package com.betvictor.randomtext;

public class RandomTextResponse {
	private String type;
	private int amount;
	private int number;
	private int numberMax;
	private String format;
	private String time;
	private String textOut;
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public int getNumberMax() {
		return numberMax;
	}
	public void setNumberMax(int numberMax) {
		this.numberMax = numberMax;
	}
	public String getFormat() {
		return format;
	}
	public void setFormat(String format) {
		this.format = format;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getTextOut() {
		return textOut;
	}
	public void setTextOut(String textOut) {
		this.textOut = textOut;
	}
	
	@Override
	public String toString() {
		return "RandomTextResponse [type=" + type + ", amount=" + amount + ", number=" + number + ", numberMax="
				+ numberMax + ", format=" + format + ", time=" + time + ", textOut=" + textOut + "]";
	}
	
}
