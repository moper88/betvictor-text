package com.betvictor.text;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.betvictor.randomtext.ProcessedRandomText;
import com.betvictor.randomtext.RandomTextService;

@RunWith(MockitoJUnitRunner.class)
public class TextDelegateTest {
	
	@Mock
	private RandomTextService randomTextService;
	
	@Mock
	private TextRepository textRepository; 
	
	@Mock
	private CompletableFuture<ProcessedRandomText> future;
	
	@InjectMocks
	private TextDelegate delegate;
	
	@Test
	public void shouldGetTextResponse() {
		given(randomTextService.getProcessedRandomTextAsync(anyInt(), eq(10), eq(20), any()))
			.willAnswer((args) -> {
				ProcessedRandomText arg = (ProcessedRandomText)args.getArgument(3);
				//Always set the same values
				arg.setWords(Arrays.asList("much", "very", "much"));
				arg.setNumberOfParagraphs(1);
				arg.setProcessingTime(1000);
				return future;
			});
		
		TextResponse response = delegate.getTextResponse(1, 5, 10, 20);
		
		//Check response
		assertNotNull(response);
		assertEquals("much", response.getFreqWord());
		assertEquals(3.0, response.getAvgParagraphSize(),0.1);
		assertEquals(1000, response.getAvgParagraphProcessingTime());
		
		//Check we call each process and we join each async task
		verify(randomTextService).getProcessedRandomTextAsync(eq(1), eq(10), eq(20), any());
		verify(randomTextService).getProcessedRandomTextAsync(eq(2), eq(10), eq(20), any());
		verify(randomTextService).getProcessedRandomTextAsync(eq(3), eq(10), eq(20), any());
		verify(randomTextService).getProcessedRandomTextAsync(eq(4), eq(10), eq(20), any());
		verify(randomTextService).getProcessedRandomTextAsync(eq(5), eq(10), eq(20), any());
		verify(future, times(5)).join();
	}
	
	@Test
	public void shouldGetEmptyHistory() {
		List<TextEntity> entityList = new ArrayList<>();
		given(textRepository.findTop10ByOrderByDateDesc()).willReturn(entityList);
		
		List<TextResponse> response = delegate.getHistory();
		
		assertNotNull(response);
		assertEquals(0, response.size());
	}
	
	@Test
	public void shouldGetHistoryWith1Item() {
		List<TextEntity> entityList = new ArrayList<>();
		TextEntity entity = new TextEntity();
		entityList.add(entity);
		given(textRepository.findTop10ByOrderByDateDesc()).willReturn(entityList);
		
		List<TextResponse> response = delegate.getHistory();
		
		assertNotNull(response);
		assertEquals(1, response.size());
		assertTrue(response.contains(entity.toTextResponse()));
	}
	
	@Test
	public void shouldGetHistoryWith3Item() {
		List<TextEntity> entityList = new ArrayList<>();
		TextEntity entity1 = new TextEntity();
		entity1.setFreqWord("one");
		entityList.add(entity1);
		TextEntity entity2 = new TextEntity();
		entity2.setFreqWord("two");
		entityList.add(entity2);
		TextEntity entity3 = new TextEntity();
		entity3.setFreqWord("three");
		entityList.add(entity3);
		given(textRepository.findTop10ByOrderByDateDesc()).willReturn(entityList);
		
		List<TextResponse> response = delegate.getHistory();
		
		assertNotNull(response);
		assertEquals(3, response.size());
		assertTrue(response.contains(entity1.toTextResponse()));
		assertTrue(response.contains(entity2.toTextResponse()));
		assertTrue(response.contains(entity3.toTextResponse()));
	}
}
