package com.betvictor.randomtext;

import java.util.ArrayList;
import java.util.List;

public class ProcessedRandomText {
	private List<String> words;
	
	private long processingTime;
	
	private int numberOfParagraphs;
	
	public ProcessedRandomText() {
		super();
	}

	public ProcessedRandomText(List<String> words, long processingTime, int numberOfParagraphs) {
		super();
		this.words = words;
		this.processingTime = processingTime;
		this.numberOfParagraphs = numberOfParagraphs;
	}

	public List<String> getWords() {
		if (words == null) {
			words = new ArrayList<>();
		}
		return words;
	}

	public void setWords(List<String> words) {
		this.words = words;
	}

	public long getProcessingTime() {
		return processingTime;
	}

	public void setProcessingTime(long processingTime) {
		this.processingTime = processingTime;
	}

	public int getNumberOfParagraphs() {
		return numberOfParagraphs;
	}

	public void setNumberOfParagraphs(int numberOfParagraphs) {
		this.numberOfParagraphs = numberOfParagraphs;
	}

	@Override
	public String toString() {
		return "ProcessedRandomText [words=" + words + ", processingTime=" + processingTime
				+ ", numberOfParagraphs=" + numberOfParagraphs + "]";
	}

}
